﻿namespace Shopcuatoi.Web.Areas.Admin.ViewModels.Products
{
    public class ProductMediaVm
    {
        public long Id { get; set; }

        public string MediaUrl { get; set; }
    }
}