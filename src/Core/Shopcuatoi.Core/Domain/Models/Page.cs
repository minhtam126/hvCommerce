﻿using Shopcuatoi.Core.Domain.Models;

namespace Shopcuatoi.Core.Domain.Models
{
    public class Page : Content
    {
        public string Body { get; set; }
    }
}